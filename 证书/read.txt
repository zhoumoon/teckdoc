keytool -genkeypair -keyalg RSA -keysize 2048 -sigalg SHA1withRSA -validity 3600 -alias myCertificate -keystore myKeystore.keystore
一个keystore应该是可以存储多套<私钥-数字证书>的信息，通过别名来区分。
通过实践，调用上述命令两次（别名不同），生成同一个keystore，用不同别名进行加密解密和签名验签，
没有任何问题。
KeyTool与本地密钥库相关联，将私钥存于密钥库，公钥则以数字证书输出。KeyTool位于JDK目录下的bin目录中，需要通过命令行进行相应的操作。
keytool -list -v -keystore myKeystore.keystore//查看keystore条目
keytool -exportcert -alias myCertificate -keystore myKeystore.keystore -file myCer.cer 
keytool -importcert -trustcacerts -alias myCertificate -file myCer.cer -keystore myKeystore.keystore
keytool -printcert -file "myCer.cer"//查看证书详情