(function() {
    var b = [9999, 9999],
        f = [9999, 9999],
        r = 0,
        s = 0,
        p, a, m, o = 0,
        d = document,
        h = [-1, -1],
        n = [-1, -1],
        c = 0,
        l = 0;
    if (typeof d.clientWidth != "number") {
        d = d.documentElement;
        if (!d || d.clientHeight === 0) {
            d = document.body
        }
    }
    $s.$b(window, ["scroll"], [
        function() {
            o++
        }
    ]);

    function j(i) {
        r = $s.t();
        b = [i.clientX, i.clientY]
    }

    function e(i) {
        s = $s.t() - r;
        f = [i.clientX, i.clientY]
    }

    function q(i) {
        if (c == 0) {
            h = [i.clientX, i.clientY];
            c = 1
        }
    }

    function k(i) {
        if (l == 0) {
            n = [i.clientX, i.clientY];
            l = 1
        }
    }
    p = $s.$$(document.body, "a");
    for (var m = 0; m < p.length; m++) {
        if (p[m].href && p[m].href.indexOf("www.sogou.com/bill_") > 0) {
            $s.$b(p[m], ["mouseover", "mousedown"], [
                function(t) {
                    j(t)
                },
                function(t) {
                    e(t)
                }
            ])
        }
    }

    function g(w, t, i) {
        if (w.indexOf("&" + t + "=") == -1) {
            w += "&" + t + "=" + i
        } else {
            var u = new RegExp("&" + t + "=[0-9,]*");
            w = w.replace(u, "&" + t + "=" + i)
        }
        return w
    }
    $s.$b($s.$("promotion_adv_container"), ["mouseover"], [
        function(t) {
            q(t)
        }
    ]);
    $s.$b($s.$("right"), ["mouseover"], [
        function(t) {
            k(t)
        }
    ]);
    window.sogou_adclk = function(i, x) {
        var v = 0;
        if (typeof sogou_last_mousedown_time != "undefined") {
            v = sogou_last_mousedown_time
        }
        if (typeof(uigs_para) != "undefined" && x.href) {
            var u = x.href;
            try {
                u = g(u, "ml", uigs_para.mml);
                u = g(u, "mc", (new Date().getTime() - v));
                u = g(u, "ma", [s, o, b[0], b[1], f[0], f[1], d.clientWidth, d.clientHeight].join(","));
                u = g(u, "block", [h[0], h[1], n[0], n[1]].join(","));
                x.href = u
            } catch (w) {}
        }
        return true
    }
})();