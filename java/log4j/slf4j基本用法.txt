slf4j与logback最佳实践
导入jar包 sfl4j-api.jar;logback-core.jar;logback-access.jar;logback-classic.jar
编写配置文件logback.xml
初始化日志：
		ILoggerFactory loggerFactory = LoggerFactory.getILoggerFactory();
		LoggerContext loggerContext = (LoggerContext) loggerFactory;
		loggerContext.reset();
		JoranConfigurator configurator = new JoranConfigurator();
		configurator.setContext(loggerContext);
		configurator.doConfigure("logback.xml");

输出日志：
Logger logger = LoggerFactory.getLogger(this.getClass());
logger.info("log infomation");
